///////////////////////////////////////////////////////mali////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file factory.hpp
/// @version 1.0
///
/// Generates random animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   09_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

using namespace std;

namespace animalfarm {

int         randomNumber();
std::string randomName( int n );
enum Color  randomColor();
enum Gender randomGender();
bool        randomBool();
float       randomWeight();

//animal generation
class AnimalFactory {
public:
   //creates Animal
   static Animal* getRandomAnimal();
   
   //deletes Animal
   void deleteAnimal(Animal* a);

};//end class


}// namespace animalfarm
