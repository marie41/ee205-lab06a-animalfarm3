///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   02_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
//#include <string>
#include <array>
#include <list>

#include "animal.hpp"
#include "mammal.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
	
#define SIZE_OF_FARM (30)
#define NUMBER_OF_ANIMALS (25)
   
   srand (time (NULL) ); //seed rand() using current time

	array <Animal*, SIZE_OF_FARM> animalArray;

	for( int i = 0 ; i < NUMBER_OF_ANIMALS ; i++ ) {
		animalArray[i] = AnimalFactory::getRandomAnimal();
	}
	
	cout << endl;
	cout << "Array of Animals:" << endl;
	cout << "   Is it empty:" << animalArray.empty() << endl;
	cout << "   Number of elements:" << animalArray.size() << endl;
	cout << "   Max size:" << animalArray.max_size() << endl;
	
	cout << endl;
	cout << "The array of animals sounds like this:" << endl;

   for (int i = 0; i < NUMBER_OF_ANIMALS ; i++) {

      cout << "   " << animalArray[i]->speak() << endl;
   }


// Having trouble getting this working.. generates a segmentation fault
/*
   cout << "debug2" << endl;
   for( auto i = animalArray.begin() ; i != animalArray.end() ; ++i ) {
      cout << "   " << (*i) -> speak() << endl;
	}
*/
	
   //list time
	list <Animal*> animalList;

	for( int i = 0 ; i < NUMBER_OF_ANIMALS ; i++ ) {
		animalList.push_front( AnimalFactory::getRandomAnimal() );
	}
	
	cout << endl;
	cout << "List of Animals:" << endl;
	cout << "   Is it empty:" << animalList.empty()<< endl;
	cout << "   Number of elements:" << animalList.size() << endl;
	cout << "   Max size:" << animalList.max_size() << endl;
	
	cout << endl;
	cout << "The list of animals sounds like this::" << endl;
	
   for( auto i = animalList.begin() ; i != animalList.end() ; ++i ) {
		cout << "   " << (*i)->speak() << endl;
	}

   // cout << "debug2" << endl;
   //deletes animals in array
   for (int i = 0; i < NUMBER_OF_ANIMALS ; i++) {

      animalArray[i] -> Animal:: deleteAnimal( animalArray[i] );
   }

   //fills array with null pointers
   std::fill( animalArray.begin() , animalArray.end() , nullptr );
   //deletes all elements in list
   for (Animal* ptr : animalList) delete ptr;
   animalList.clear();
   //cout << animalList.size() << endl;
   if (animalArray[0] == nullptr && animalList.empty())
      cout << endl << "Farewell Animals great and small" << endl;

	return 0;
}
