///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 1.0
///
/// Tests random generator functions
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   02_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
//#include "factory.hpp"

using namespace std;
using namespace animalfarm;
/*
#define MAX_NAME_LENGTH (9)
std::string randomName() {
   srand (time (NULL) ); //seed rand() using current time
   int length = rand() % 6 + 4; //length of name is random integer between 4 and 9
   
   char name[ MAX_NAME_LENGTH ]; //declare name array 
   name[0] = 'A' + ( rand() % 26 ); //Add first letter, uppercase 
   
   for ( int i = 1; i < length ; i++ ) {
      name[i] = 'a' + ( rand() % 26 );
   }

   return name;
}
*/

int main() {
	cout << "Welcome" << endl;
   srand (time (NULL) ); //seed rand() using current time

   Cat testCat( randomName( randomNumber() ), randomColor(), randomGender() );
	cout << "Random Name = " << testCat.name << endl;
	cout << "Random Color = " << testCat.colorName( testCat.hairColor ) << endl;
	cout << "Random Gender = " << testCat.genderName( testCat.gender ) << endl;
	Cat* pCat = &testCat;
   cout << testCat.speak() << endl;
   cout << pCat->speak() << endl;

	cout << "ANIMAL FACTORY TEST" << endl;

	Animal* pTestAnimal = AnimalFactory::getRandomAnimal();
   pTestAnimal->printInfo();
   cout << pTestAnimal->speak() << endl;
   pTestAnimal -> deleteAnimal(pTestAnimal);

 //  cout << "25 animal sounds" << endl;
//   for( int i = 0; i < 25 ; i++ ) {
  //    Animal* a = AnimalFactory::getRandomAnimal();
    //  cout << a->speak() << endl;
    //  a->deleteAnimal(a);
  // }

	return 0;
}//end of main

