///////////////////////////////////////////////////////mali////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   02_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };

//factory functions
int         randomNumber();
std::string randomName( int n );
enum Color  randomColor();
enum Gender randomGender();
bool        randomBool();
float       randomWeight();

class Animal {
public:
   //constructor and destructor
   Animal();
   ~Animal();

	enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
   void deleteAnimal(Animal* a);
   
   virtual void printInfo();
	
	string colorName  (enum Color color);
	string genderName (enum Gender gender);

};//end Animal

//animal generation
class AnimalFactory {
public:
   //creates Animal
   static Animal* getRandomAnimal();

   //deletes Animal
//   void deleteAnimal(Animal* a);

};//end class

}// namespace animalfarm
