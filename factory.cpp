///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file factory.cpp
/// @version 1.0
///
/// Generate random animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   09_MAR_2021
//////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstring>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "factory.hpp"

using namespace std;

namespace animalfarm {

int randomNumber () {
   srand (time (NULL) ); //seed rand() using current time
   return rand() % 6; //random integer from 0 to 5
}

#define MAX_NAME_LENGTH (10)
//returns random name of length 4-9. Expecting num between 0 and 5 
std::string randomName  (int num) {
   srand (time (NULL) );   //seed rand() using current time
   int length = num + 4;   //length of name is random integer between 4 and 9
   char name[ length + 1 ]; //declare name array, +1 size for '\0' termination  
   name[0] = 'A' + ( rand() % 26 ); //Add first letter, uppercase 
   
   for ( int i = 1; i < length ; i++ ) {
      name[i] = 'a' + ( rand() % 26 );
   }
   name[ length ] = '\0'; //making sure name is '\0' terminated
  
   return name;
}

enum Color randomColor() {
   srand (time (NULL) ); //seed rand() using current time
   enum Color color = (enum  Color) (rand() % 6); //random color generated from random integer between 0 and 6

   return color;
}
;
enum Gender randomGender() {
   srand (time (NULL) ); //seed rand() using current time
   enum Gender gender = (enum Gender) (rand() % 2); //gender is male or female
   
      return gender;
}

bool randomBool() {
   srand (time (NULL) ); //seed rand() using current time
   bool randomBool = (bool) (rand() % 2);
   
   return randomBool;
}

float randomWeight() {
   srand (time (NULL) ); //seed rand() using current time
   float weight = (float) (rand() % 20) + 70; // number between 70 and 90
   weight += ((rand() % 10) /10); //adds random after decimal number

   return weight;
}

//creates a random animal
Animal* AnimalFactory::getRandomAnimal () {
   Animal* pAnimal;
   srand (time (NULL) ); //seed rand() using current time
   int i = rand() % 6; //species is tied to random integer from 0 to 5

   switch (i){
      case 0: pAnimal = new Cat(      randomName( randomNumber() ),  randomColor(), randomGender() );
      case 1: pAnimal = new Dog(      randomName( randomNumber() ),  randomColor(), randomGender() );
      case 2: pAnimal = new Nunu(     randomBool(),                  randomColor(), randomGender() );
      case 3: pAnimal = new Aku(      randomWeight(),                randomColor(), randomGender() );
      case 4: pAnimal = new Palila(   randomName( randomNumber() ),  randomColor(), randomGender() );
      case 5: pAnimal = new Nene(     randomName( randomNumber() ),  randomColor(), randomGender() );
   }
   
   //prints . on creation of new animal   
   cout << "." << endl;

   //DEBUG
   pAnimal->printInfo();

   return pAnimal;

}

void AnimalFactory::deleteAnimal (Animal* byeAnimal) {
   delete[] byeAnimal;

   //prints x when animal is deleted
   cout << "x" << endl;

}

} // namespace animalfarm
